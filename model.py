# -*- coding: utf-8 -*-
import os
import keras
from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from keras.applications.resnet50 import ResNet50
from keras.applications.inception_v3 import InceptionV3
from keras.applications.densenet import DenseNet121
from keras.applications.xception import Xception
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.applications.nasnet import NASNetLarge
from keras.preprocessing import image
import numpy as np
from keras.models import Model,load_model
from keras.layers import Dense,Input,Subtract,Concatenate,Dropout,Flatten,Activation,concatenate
from keras.optimizers import Adam, SGD, RMSprop
from keras.callbacks import ModelCheckpoint,TensorBoard
from keras.models import Sequential
from keras.layers.convolutional import Convolution2D, Conv2D
from keras.layers.pooling import AveragePooling2D, GlobalAveragePooling2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.merge import add, multiply
    
class Models:    
    def __init__(self, checkpoint_path, loss, dataset_name):
        self.checkpoint_path = checkpoint_path
        self.loss = loss
        self.dataset_name = dataset_name


    def Vgg16_model_imagenet(self):
        '''
        将vgg16的全连接层全部去掉，保留剩余的网络结构和权重；
        新添加3个全连接层，权重随机初始化；
        最后，对整个网络进行训练
        '''
        vgg16_model = VGG16(weights='imagenet', include_top=False)
        vgg16_model.summary()

        # 建立多输入的模型
        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input, right_input])
        concatenated = Concatenate(axis=-1)([subtracted,subtracted,subtracted])

        out = vgg16_model(concatenated)
        out = Flatten(name="flatten")(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        predictions = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=predictions)

        model.summary()

        return model


    def Vgg16_model_fine_tune(self):
        vgg16_model = VGG16(weights=None, include_top=False)

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted, left_input, right_input])

        out = vgg16_model(concatenated)
        out = Flatten(name="flatten")(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)
        model.load_weights(os.path.join(self.checkpoint_path, 'Vgg16_model_imagenet_'
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)

        model.summary()

        return model

    def Vgg16_model_new_one(self):
        vgg16_model = VGG16(weights=None, include_top=False)
        
        non_original_left = Input(shape=(224,224,1))
        non_original_right = Input(shape=(224,224,1))
        original_left = Input(shape=(224,224,1))
        original_right = Input(shape=(224,224,1))

        subtracted = Subtract()([non_original_left, non_original_right])
        concatenated = Concatenate(axis=-1)([subtracted, original_left, original_right])

        out = vgg16_model(concatenated)
        out = Flatten(name="flatten")(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[non_original_left, non_original_right, original_left, original_right], outputs=out)
        model.load_weights(os.path.join(self.checkpoint_path, 'Vgg16_model_imagenet_'
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)

        model.summary()

        return model

    def Vgg16_model_new_two(self):
        vgg16_model = VGG16(weights=None, include_top=False)
        
        non_original_left = Input(shape=(224,224,1))
        non_original_right = Input(shape=(224,224,1))
        original_left = Input(shape=(224,224,1))
        original_right = Input(shape=(224,224,1))

        subtracted = Subtract()([original_left, original_right])
        concatenated = Concatenate(axis=-1)([subtracted, non_original_left, non_original_right])

        out = vgg16_model(concatenated)
        out = Flatten(name="flatten")(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[non_original_left, non_original_right, original_left, original_right], outputs=out)
        model.load_weights(os.path.join(self.checkpoint_path, 'Vgg16_model_imagenet_'
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)

        model.summary()

        return model

    def ResNet50_model_imagenet(self):
        ResNet50_model = ResNet50(weights='imagenet', include_top=False)
        ResNet50_model.summary()

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted,subtracted,subtracted])

        out = ResNet50_model(concatenated)
        out = Flatten(name="flatten")(out)
        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)

        return model  


    def ResNet50_model_fine_tune(self):
        ResNet50_model = ResNet50(weights=None, include_top=False)

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted, left_input, right_input])

        out = ResNet50_model(concatenated)
        out = Flatten(name="flatten")(out)

        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)
        model.load_weights(os.path.join(self.checkpoint_path, 'ResNet50_model_imagenet_'
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)

        model.summary()

        return model


    def Vgg19_model_imagenet(self):
        vgg19_model = VGG19(weights='imagenet')

        x = vgg19_model.get_layer('flatten').output
        x =  Dense(4096, activation='relu')(x)
        x = Dropout(0.5)(x)
        x =  Dense(4096, activation='relu')(x)
        x = Dropout(0.5)(x)
        predictions = Dense(2, activation='softmax',name='predictions')(x)

        base_model = Model(inputs=vgg19_model.input, outputs=predictions)

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted,subtracted,subtracted])

        out = base_model(concatenated)

        model = Model(inputs=[left_input, right_input], outputs=out)

        return model


    def Vgg19_model_fine_tune(self):
        VGG19_model = VGG19(weights=None, include_top=False)

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted, left_input, right_input])

        out = VGG19_model(concatenated)
        out = Flatten(name="flatten")(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        out = Dense(4096, activation='relu')(out)
        out = Dropout(0.5)(out)
        predictions = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=predictions)
        model.load_weights(os.path.join(self.checkpoint_path, "Vgg19_model_imagenet_"
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)

        model.summary()

        return model


    def InceptionV3_model_imagenet(self):
        InceptionV3_model = InceptionV3(weights='imagenet', include_top=False)
        InceptionV3_model.summary()

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted,subtracted,subtracted])

        out = InceptionV3_model(concatenated)
        out = Flatten(name="flatten")(out)
        out = Dense(2, activation='softmax', name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)

        return model
    
    
    def InceptionV3_model_fine_tune(self):
        InceptionV3_model = InceptionV3(weights=None, include_top=False)

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted, left_input, right_input])

        out = InceptionV3_model(concatenated)
        out = Flatten(name="flatten")(out)

        out = Dense(2, activation='softmax', name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)
        model.load_weights(os.path.join(self.checkpoint_path, "InceptionV3_model_imagenet_"
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)

        return model


    def DenseNet_model_imagenet(self):
        DenseNet121_model = DenseNet121(weights='imagenet', include_top=False)

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted,subtracted,subtracted])

        out = DenseNet121_model(concatenated)

        out = GlobalAveragePooling2D()(out)
        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)
        model.summary()

        return model
    
    
    def DenseNet_model_fine_tune(self):
        DenseNet121_model = DenseNet121(weights=None, include_top=False)

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted, left_input, right_input])

        out = DenseNet121_model(concatenated)

        out = GlobalAveragePooling2D()(out)
        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)
        model.load_weights(os.path.join(self.checkpoint_path, "DenseNet_model_imagenet_"
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)
        model.summary()

        return model


    def Xception_model_imagenet(self):
        Xception_model = Xception(weights='imagenet', include_top=False)

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted,subtracted,subtracted])

        out = Xception_model(concatenated)
        out = Flatten(name="flatten")(out)

        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)
        model.summary()

        return model
    
    
    def Xception_model_fine_tune(self):
        Xception_model = Xception(weights=None, include_top=False)

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted, left_input, right_input])

        out = Xception_model(concatenated)
        out = Flatten(name="flatten")(out)

        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)
        model.load_weights(os.path.join(self.checkpoint_path, "Xception_model_imagenet_"
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)
        model.summary()

        return model


    def InceptionResNetV2_model_imagenet(self):
        InceptionResNetV2_model = InceptionResNetV2(weights='imagenet', include_top=False, pooling='avg')

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted,subtracted,subtracted])

        out = InceptionResNetV2_model(concatenated)

        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)
        model.summary()

        return model
    
    
    def InceptionResNetV2_model_fine_tune(self):
        InceptionResNetV2_model = InceptionResNetV2(weights=None, include_top=False, pooling='avg')

        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))

        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted, left_input, right_input])

        out = InceptionResNetV2_model(concatenated)

        out = Dense(2, activation='softmax',name='predictions')(out)

        model = Model(inputs=[left_input, right_input], outputs=out)
        model.load_weights(os.path.join(self.checkpoint_path, "InceptionResNetV2_model_imagenet_"
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)
        model.summary()

        return model
    
    
    def NASNetLarge_model_imagenet(self):
        NASNetLarge_model = NASNetLarge(weights='imagenet', include_top=False)
        NASNetLarge(weights=None, include_top=True).summary()
    
        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))
    
        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted,subtracted,subtracted])

        out = NASNetLarge_model(concatenated)
        out = Flatten(name="flatten")(out)

        out = Dense(2, activation='softmax',name='predictions')(out)
    
        model = Model(inputs=[left_input, right_input], outputs=out)
        model.summary()
    
        return model

    
    def NASNetLarge_model_fine_tune(self):
        NASNetLarge_model = NASNetLarge(weights=None, include_top=False)
    
        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))
    
        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted, left_input, right_input])

        out = NASNetLarge_model(concatenated)
        out = Flatten(name="flatten")(out)

        out = Dense(2, activation='softmax',name='predictions')(out)
    
        model = Model(inputs=[left_input, right_input], outputs=out)
        model.load_weights(os.path.join(self.checkpoint_path, "NASNetLarge_model_imagenet_"
         + self.loss + "_" + self.dataset_name + '.h5'), by_name=True)
        model.summary()
    
        return model


    def WDNet_model(self):
        k_size = 3
        nb_pool=(2, 2)
        downsize_nb_filters_factor=2
        left_input = Input(shape=(224,224,1))
        right_input = Input(shape=(224,224,1))
        subtracted = Subtract()([left_input,right_input])
        concatenated = Concatenate(axis=-1)([subtracted,subtracted,subtracted])
        
        def Inception_block(nb_filter, x):
            tower_1 = Conv2D(nb_filter, (1,1), padding='same', activation='relu')(x)
            tower_1 = Conv2D(nb_filter, (3,3), padding='same', activation='relu')(tower_1)
            tower_2 = Conv2D(nb_filter, (1,1), padding='same', activation='relu')(x)
            tower_2 = Conv2D(nb_filter, (5,5), padding='same', activation='relu')(tower_2)
            tower_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(x)
            tower_3 = Conv2D(nb_filter, (1,1), padding='same', activation='relu')(tower_3)

            output = concatenate([tower_1, tower_2, tower_3], axis=3)

            return output
        
        def RCL_block(out_num_filters, l, pool=False):
            conv1 = Conv2D(out_num_filters, 1, padding='same')
            stack1 = conv1(l)
            stack2 = BatchNormalization()(stack1)
            stack3 = Activation('relu')(stack2)

            conv2 = Conv2D(out_num_filters, k_size, padding='same', kernel_initializer='he_normal')
            stack4 = conv2(stack3)
            stack5 = add([stack1, stack4])
            stack6 = BatchNormalization()(stack5)
            stack7 = Activation('relu')(stack6)

            conv3 = Conv2D(out_num_filters, k_size, padding='same')
            stack8 = conv3(stack7)
            stack9 = add([stack1, stack8])
            stack10 = BatchNormalization()(stack9)
            stack11 = Activation('relu')(stack10)

            conv4 = Conv2D(out_num_filters, k_size, padding='same')
            stack12 = conv4(stack11)
            stack13 = add([stack1, stack12])
            stack14 = BatchNormalization()(stack13)
            stack15 = Activation('relu')(stack14)

            if pool:
                stack16 = MaxPooling2D((2, 2), border_mode='same')(stack15)
                stack17 = Dropout(0.1)(stack16)
            else:
                stack17 = stack15

            return stack17
        
        conv1 = Conv2D(int(64 / downsize_nb_filters_factor), (3, 3), padding='same')(concatenated)
        conv1 = Activation('relu')(conv1)
        conv1 = BatchNormalization()(conv1)

        inception1 = Inception_block(int(64 / downsize_nb_filters_factor), conv1)
        inception2 = Inception_block(int(64 / downsize_nb_filters_factor), inception1)
        rcl1 = RCL_block(int(64 / downsize_nb_filters_factor), inception2, pool=False)
        pool1 = MaxPooling2D(pool_size=nb_pool)(rcl1)

        inception3 = Inception_block(int(128 / downsize_nb_filters_factor), pool1)
        inception4 = Inception_block(int(128 / downsize_nb_filters_factor), inception3)
        rcl2 = RCL_block(int(128 / downsize_nb_filters_factor), inception4, pool=False)
        pool2 = MaxPooling2D(pool_size=nb_pool)(rcl2)

        inception5 = Inception_block(int(256 / downsize_nb_filters_factor), pool2)
        inception6 = Inception_block(int(256 / downsize_nb_filters_factor), inception5)
        rcl3 = RCL_block(int(256 / downsize_nb_filters_factor), inception6, pool=False)
        pool3 = MaxPooling2D(pool_size=nb_pool)(rcl3)

        inception7 = Inception_block(int(512 / downsize_nb_filters_factor), pool3)
        inception8 = Inception_block(int(512 / downsize_nb_filters_factor), inception7)
        rcl4 = RCL_block(int(512 / downsize_nb_filters_factor), inception8, pool=False)
        pool4 = MaxPooling2D(pool_size=nb_pool)(rcl4)

        inception9 = Inception_block(int(1024 / downsize_nb_filters_factor), pool4)
        inception10 = Inception_block(int(1024 / downsize_nb_filters_factor), inception9)
        inception11 = Inception_block(int(1024 / downsize_nb_filters_factor), inception10)
        rcl5 = RCL_block(int(1024 / downsize_nb_filters_factor), inception11, pool=False)

        out = Flatten(name="flatten")(rcl5)

        out = Dense(2, activation='softmax',name='predictions')(out)
    
        model = Model(inputs=[left_input, right_input], outputs=out)
        model.summary()

        return model
