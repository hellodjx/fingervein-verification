import configparser
import glob
import os
import random
from itertools import combinations
from random import choice

TXT_FOLDER = "./dataset_txt"


def get_txt_file_path(file_name):
    config = configparser.ConfigParser()
    config.read("./config.cfg")
    dataset_name = config["SETTINGS"]['dataset']

    return os.path.join(TXT_FOLDER, dataset_name + "_" + file_name)


def get_fingerDirs():
    config = configparser.ConfigParser()
    config.read("./config.cfg")
    dataset_name = config["SETTINGS"]['dataset']
    dataset_path = os.path.join(config["SETTINGS"]['dataset_folder'], dataset_name)

    finger_folder = []

    def find_finger_folder(path):
        file_list = list(glob.glob(os.path.join(path, "*")))
        for i in file_list:
            if os.path.isdir(i):
                find_finger_folder(i)
            else:
                parent_path = os.path.dirname(i)
                finger_folder.append(parent_path)

    find_finger_folder(dataset_path)
    finger_folder = set(finger_folder)
    print(len(finger_folder))

    with open('./train_finger_dirs.txt', 'w') as f:
        for line in finger_folder:  # line依次表示finger_dirs中的元素
            f.write(line + '\n')


def get_pos_samples():
    finger_dirs = []
    with open('./train_finger_dirs.txt', 'r') as f:
        finger_dirs = f.readlines()  # 用于读取所有行并返回列表
        # rstrip() 删除字符串末尾的指定字符，去掉'\n'
        finger_dirs = [item.rstrip('\n') for item in finger_dirs]
    positive_sample_list = []
    for finger_path in finger_dirs:
        # img依次表示列表中的元素，每类有10张图像
        one_class = [os.path.join(finger_path, img) for img in os.listdir(finger_path)]
        positive_sample = list(combinations(one_class, 2))  # 从10张中任选2张，以组合的方式
        for item in positive_sample:
            sample = item[0] + ';' + item[1] + '\n'
            positive_sample_list.append(sample)

    print(len(positive_sample_list))

    random.shuffle(positive_sample_list)
    train_positive_sample_list = positive_sample_list[0: int(
        len(positive_sample_list) * 0.8)]
    test_positive_sample_list = positive_sample_list[int(
        len(positive_sample_list) * 0.8):]
    if os.path.exists(get_txt_file_path('train_positive_pairs.txt')) or os.path.exists(
            get_txt_file_path('test_positive_pairs.txt')):
        answer = input("txt文件已存在，是否要覆盖（如果想要转换txt文件里的路径，请运行convert_txt.py），输入yes或no回答：")
        if answer != 'yes':
            print("终止生成文件，原文件未被覆盖")
            exit()
    with open(get_txt_file_path('train_positive_pairs.txt'), 'w') as f:
        f.writelines(train_positive_sample_list)
    with open(get_txt_file_path('test_positive_pairs.txt'), 'w') as f:
        f.writelines(test_positive_sample_list)


def get_neg_samples():
    finger_dirs = []
    with open('./train_finger_dirs.txt', 'r') as f:
        finger_dirs = f.readlines()  # 用于读取所有行并返回列表
        # rstrip() 删除字符串末尾的指定字符，去掉'\n'
        finger_dirs = [item.rstrip('\n') for item in finger_dirs]

    neg_list = list(combinations(finger_dirs, 2))
    print(len(neg_list))

    negtive_sample_list = []
    for item in neg_list:
        fn_0 = os.listdir(item[0])
        left_img = os.path.join(item[0], choice(fn_0))  # 从该手指随机选择1张图像

        fn_1 = os.listdir(item[1])
        right_img = os.path.join(item[1], choice(fn_1))

        sample = left_img + ';' + right_img + '\n'
        negtive_sample_list.append(sample)

    random.shuffle(negtive_sample_list)
    train_negtive_sample_list = negtive_sample_list[0: int(
        len(negtive_sample_list) * 0.8)]
    test_negtive_sample_list = negtive_sample_list[int(
        len(negtive_sample_list) * 0.8):]
    if os.path.exists(get_txt_file_path('train_negtive_pairs.txt')) or os.path.exists(
            get_txt_file_path('test_negtive_pairs.txt')):
        answer = input("txt文件已存在，是否要覆盖（如果想要转换txt文件里的路径，请运行convert_txt.py），输入yes或no回答：")
        if answer != 'yes':
            print("终止生成文件，原文件未被覆盖")
            exit()
    with open(get_txt_file_path('train_negtive_pairs.txt'), 'w') as f:
        f.writelines(train_negtive_sample_list)
    with open(get_txt_file_path('test_negtive_pairs.txt'), 'w') as f:
        f.writelines(test_negtive_sample_list)


if __name__ == '__main__':
    if not os.path.exists(TXT_FOLDER):
        os.makedirs(TXT_FOLDER)
    get_fingerDirs()
    get_pos_samples()
    get_neg_samples()
