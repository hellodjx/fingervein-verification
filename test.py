import configparser
from os.path import join as path_join

import cv2
import sys
import numpy as np
from keras.models import Model, load_model
from loss import *
from load_data import get_original


RESULT_DIR = "./result"
TXT_FOLDER = "./dataset_txt"


def read_img_V2(img_path):
    img = cv2.imread(img_path,0) # img的形状为(w,h)
    img = cv2.resize(img,(224, 224))
    x = np.expand_dims(img, axis=0)
    x = x.reshape(1,224,224,1)
    x = x.astype('float32')
    x /= 255
    return x


def get_txt_file_path(file_name):
    config = configparser.ConfigParser()
    config.read("./config.cfg")
    dataset_name = config["SETTINGS"]['dataset']

    return path_join(TXT_FOLDER, dataset_name + "_" + file_name)

    
def get_pos_sim(model, model_name, loss, dataset_name, data_input_type):
    sampels = []
    with open(get_txt_file_path('test_positive_pairs.txt'), 'r') as f:
        sampels = f.readlines()
    
    with open(path_join(RESULT_DIR, model_name + "_" + loss + "_" + dataset_name + '_positive_sim.txt'), 'w') as f:
        for i in range(0, len(sampels)):
            sampels[i] = sampels[i].rstrip('\n')
            sample = sampels[i].split(';')
            
            img_path_1 = sample[0]
            img_path_2 = sample[1]
            
            img1 = read_img_V2(img_path_1)
            img2 = read_img_V2(img_path_2) 

            if data_input_type == "general":
                pro = model.predict([img1,img2])
            else:
                original_sample = get_original(sample)
                img_path_3 = original_sample[0]
                img_path_4 = original_sample[1]
                img3 = read_img_V2(img_path_3)
                img4 = read_img_V2(img_path_4)
                pro = model.predict([img1,img2,img3,img4])
            pro = pro.reshape((2,))
            p_pos = pro[1]
    
            sim = p_pos
            print(sim)
            f.write(str(sim) + '\n')

def get_neg_sim(model, model_name, loss, dataset_name, data_input_type):
    sampels = []
    with open(get_txt_file_path('test_negtive_pairs.txt'), 'r') as f:
        sampels = f.readlines()
    
    with open(path_join(RESULT_DIR, model_name + "_" + loss + "_" + dataset_name + '_negtive_sim.txt'), 'w') as f:
        for i in range(0, len(sampels)):
            sampels[i] = sampels[i].rstrip('\n')
            sample = sampels[i].split(';')
            
            img_path_1 = sample[0]
            img_path_2 = sample[1]
        
            img1 = read_img_V2(img_path_1)
            img2 = read_img_V2(img_path_2)
    
            if data_input_type == "general":
                pro = model.predict([img1,img2])
            else:
                original_sample = get_original(sample)
                img_path_3 = original_sample[0]
                img_path_4 = original_sample[1]
                img3 = read_img_V2(img_path_3)
                img4 = read_img_V2(img_path_4)
                pro = model.predict([img1,img2,img3,img4])
            pro = pro.reshape((2,))
            p_pos = pro[1]
    
            sim = p_pos
            print(sim)
            f.write(str(sim)+'\n')
            

if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read("./config.cfg")
    model_name = config["SETTINGS"]['model_name']
    checkpoint_path = config["SETTINGS"]['checkpoint_path']
    loss = config["SETTINGS"]['loss']
    dataset_name = config["SETTINGS"]['dataset']
    data_input_type = config["SETTINGS"]['data_input_type']

    model_path = path_join(checkpoint_path, model_name + "_" + loss + "_" + dataset_name + '.h5')
    if loss == "categorical_crossentropy":
        best_model = load_model(model_path)
    else:
        best_model = load_model(model_path, custom_objects={loss:FUNC_DICT[loss]})
    
    get_pos_sim(best_model, model_name, loss, dataset_name, data_input_type)
    get_neg_sim(best_model, model_name, loss, dataset_name, data_input_type)
