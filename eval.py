import configparser
from os.path import join as path_join

RESULT_DIR = "./result"
config = configparser.ConfigParser()
config.read("./config.cfg")
model_name = config["SETTINGS"]['model_name']
loss = config["SETTINGS"]['loss']
dataset_name = config["SETTINGS"]['dataset']
    
pos_sims = []
with open(path_join(RESULT_DIR, model_name + "_" + loss + "_" + dataset_name + '_positive_sim.txt'), 'r') as f:
        pos_sims = f.readlines() # pos_sims是一个list
        pos_sims = [float(item.rstrip('\n')) for item in pos_sims]
neg_sims = []
with open(path_join(RESULT_DIR, model_name + "_" + loss + "_" + dataset_name + '_negtive_sim.txt'), 'r') as f:
        neg_sims = f.readlines() # pos_sims是一个list
        neg_sims = [float(item.rstrip('\n')) for item in neg_sims]

def get_T_FRR_FAR():
    with open(path_join(RESULT_DIR, model_name + "_" + loss + "_" + dataset_name + '_T_FRR_FAR.txt'), 'w') as f:
        EER_FRR = '0'
        EER_FAR = '0'
        T = 0.0000001
        while T < 0.1:
            count = 0
            for s in pos_sims:
                if s<T:
                    count += 1
            FRR = float(count)/len(pos_sims)
            FRR = '%.3f%%' % (FRR * 100) 
        
            count_1 = 0
            for s in neg_sims:
                if s>T:
                    count_1 += 1
            FAR = float(count_1)/len(neg_sims)
            FAR = '%.3f%%' % (FAR * 100)
            
            print('T:%f  FRR:%s  FAR:%s'%(T,FRR,FAR))
            f.write('%f;%s;%s'%(T,FRR,FAR) + '\n')
            # 这里可以加上一个判断：百分号后保留2位小数时，值相等
            if round(float(FRR.rstrip('%')),2) == round(float(FAR.rstrip('%')),2):
                EER_FRR = FRR
                EER_FAR = FAR
            
            T += 0.00001
        print('EER_FRR:%s EER_FAR:%s '%(EER_FRR,EER_FAR))
            
def get_FRR():            
    with open('FRR.txt', 'w') as f:
            T = 0.3000
            while T< 0.7000:
                count = 0
                for s in pos_sims:
                    if s<T:
                        count += 1
                FRR = float(count)/len(pos_sims)
                #FRR = '%.3f%%' % (FRR * 100)
                f.write('%.3f' % (FRR * 100)+'\n')
                
                '''
                count_1 = 0
                for s in neg_sims:
                    if s>T:
                        count_1 += 1
                FAR = float(count_1)/len(neg_sims)
                FAR = '%.3f%%' % (FAR * 100)
                
                print('T:%f  FRR:%s  FAR:%s'%(T,FRR,FAR))
                f.write('T:%f  FRR:%s  FAR:%s'%(T,FRR,FAR)+'\n')
                '''
                T += 0.00001
def get_FAR():                
    with open('FAR.txt', 'w') as f:
            T = 0.3000
            while T< 0.7000:
                '''
                count = 0
                for s in pos_sims:
                    if s<T:
                        count += 1
                FRR = float(count)/len(pos_sims)
                FRR = '%.3f%%' % (FRR * 100) 
                '''
                count_1 = 0
                for s in neg_sims:
                    if s>T:
                        count_1 += 1
                FAR = float(count_1)/len(neg_sims)
                #FAR = '%.3f%%' % (FAR * 100)
                f.write('%.3f' % (FAR * 100)+'\n')
                
                #print('T:%f  FRR:%s  FAR:%s'%(T,FRR,FAR))
                #f.write('T:%f  FRR:%s  FAR:%s'%(T,FRR,FAR)+'\n')
                
                T += 0.00001
                
get_T_FRR_FAR()                
get_FRR()                
get_FAR()