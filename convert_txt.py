import os
import configparser

TXT_FOLDER = "./dataset_txt"


def get_txt_file_path(file_name):
    config = configparser.ConfigParser()
    config.read("./config.cfg")
    dataset_name = config["SETTINGS"]['dataset']

    return os.path.join(TXT_FOLDER, dataset_name + "_" + file_name)


def replace_path(path, dataset_path):
    path = path.replace("\\", "/")
    for index, item in enumerate(path.split('/')):
        try:
            item = int(item)        #因为在数据集中人的编号位于顶层
            break
        except:
            pass
    
    result_path = os.path.join(dataset_path, "/".join(path.split('/')[index:]))
    return result_path


def process_txt(txt_path, dataset_path):
    with open(txt_path, 'r') as f:
        samples = f.readlines()
        samples = [item.rstrip('\n') for item in samples]
        new_samples = []
        for i in samples:
            one, two = i.split(';')
            one = replace_path(one, dataset_path).replace('\\', '/')
            two = replace_path(two, dataset_path).replace('\\', '/')
            pairs = one + ';' + two + '\n'
            new_samples.append(pairs)

    with open(txt_path, 'w') as f:
        f.writelines(new_samples)

config = configparser.ConfigParser()
config.read("./config.cfg")
dataset_name = config["SETTINGS"]['dataset']
dataset_path = os.path.join(config["SETTINGS"]['dataset_folder'], dataset_name)

process_txt(get_txt_file_path('train_positive_pairs.txt'), dataset_path)
process_txt(get_txt_file_path('test_positive_pairs.txt'), dataset_path)
process_txt(get_txt_file_path('train_negtive_pairs.txt'), dataset_path)
process_txt(get_txt_file_path('test_negtive_pairs.txt'), dataset_path)