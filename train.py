import argparse
import configparser
import os
import sys

import keras.backend as K
import numpy as np
import tensorflow as tf
from keras.callbacks import ModelCheckpoint, TensorBoard, CSVLogger
from keras.layers import Concatenate, Dense, Dropout, Input, Subtract
from keras.models import Model
from keras.optimizers import SGD, Adam, RMSprop
from keras.preprocessing import image

from load_data import get_train_batch, load_tra_val_path
from model import *
from loss import *


def get_model(model_name, checkpoint_path, loss, dataset_name):
    Model_instance = Models(checkpoint_path, loss, dataset_name)
    model = getattr(Model_instance, model_name)()

    return model


def FV_train():
    config = configparser.ConfigParser()
    config.read("./config.cfg")
    model_name = config["SETTINGS"]['model_name']
    checkpoint_path = config["SETTINGS"]['checkpoint_path']
    loss = config["SETTINGS"]['loss']
    dataset_name = config["SETTINGS"]['dataset']
    data_input_type = config["SETTINGS"]['data_input_type']

    X_train, X_valid, y_train, y_valid = load_tra_val_path(data_input_type)
    train_batch_size = 32
    valid_batch_size = 32
    img_w = 224
    img_h = 224
    color_type = 1

    FV_model = get_model(model_name, checkpoint_path, loss, dataset_name)

    # 编译
    rms = RMSprop()
    sgd = SGD(lr=0.0001, decay=1e-6, momentum=0.9, nesterov=True)
    adm = Adam(lr=0.001)

    try:
        FV_model.compile(loss=loss, optimizer=sgd, metrics=['accuracy'])
    except:
        FV_model.compile(loss=FUNC_DICT[loss], optimizer=sgd, metrics=['accuracy'])

    # 训练时保存最佳模型、可视化
    file_name = os.path.join(checkpoint_path, model_name + "_" + loss + "_" + dataset_name + '.h5')
    if not os.path.exists(checkpoint_path):
        os.makedirs(checkpoint_path)
    print(file_name)
    log_dir = checkpoint_path

    checkpoint = ModelCheckpoint(filepath=file_name, monitor='val_acc', mode='auto')

    tbCallbacks = TensorBoard(log_dir=log_dir, histogram_freq=1, write_graph=True, write_images=True)

    csv_logger = CSVLogger(model_name + "_" + loss + "_" + dataset_name + '_train_log.csv')
    callback_lists = [checkpoint, csv_logger]

    result = FV_model.fit_generator(
        generator=get_train_batch(X_train, y_train, train_batch_size, img_w, img_h, color_type, True, data_input_type),
        steps_per_epoch=int(len(X_train) / train_batch_size),
        epochs=50,
        verbose=1,
        validation_data=get_train_batch(X_valid, y_valid, valid_batch_size, img_w, img_h, color_type, False,
                                        data_input_type),
        validation_steps=int(len(X_valid) / valid_batch_size),
        callbacks=callback_lists,
        max_queue_size=10,  # 生成器队列的最大尺寸
        workers=1)  # 使用的最大线程数量


if __name__ == '__main__':
    FV_train()
