import keras.backend as K
import numpy as np
import tensorflow as tf

margin = 0.8
theta = lambda t: (K.sign(t)+1.)/2.

def variant_crossentropy_loss(y_true, y_pred):
    return - theta(margin - y_pred) * y_true * K.log(y_pred + 1e-9) - theta(y_pred - 1 + margin) * (1 - y_true) * K.log(1 - y_pred + 1e-9)


gamma = 2
alpha = 0.5

def focal_loss_fixed(y_true, y_pred):
    epsilon = 1.e-9
    y_true = tf.convert_to_tensor(y_true, tf.float32)
    y_pred = tf.convert_to_tensor(y_pred, tf.float32)
    # to advoid numeric underflow
    model_out = tf.clip_by_value(y_pred, epsilon, 1.-epsilon)

    # compute cross entropy ce = ce_0 + ce_1 = - (1-y)*log(1-y_hat) - y*log(y_hat)
    ce_0 = tf.multiply(tf.subtract(1., y_true), -
                           tf.log(tf.subtract(1., model_out)))
    ce_1 = tf.multiply(y_true, -tf.log(model_out))

    # compute focal loss fl = fl_0 + fl_1
    # obviously fl < ce because of the down-weighting, we can fix it by rescaling
    # fl_0 = -(1-y_true)*(1-alpha)*((y_hat)^gamma)*log(1-y_hat) = (1-alpha)*((y_hat)^gamma)*ce_0
    fl_0 = tf.multiply(tf.pow(model_out, gamma), ce_0)
    fl_0 = tf.multiply(1.-alpha, fl_0)
    # fl_1= -y_true*alpha*((1-y_hat)^gamma)*log(y_hat) = alpha*((1-y_hat)^gamma*ce_1
    fl_1 = tf.multiply(tf.pow(tf.subtract(1., model_out), gamma), ce_1)
    fl_1 = tf.multiply(alpha, fl_1)
    fl = tf.add(fl_0, fl_1)
    f1_avg = tf.reduce_mean(fl)

    return f1_avg


FUNC_DICT = {"variant_crossentropy_loss":variant_crossentropy_loss
    , "focal_loss_fixed":focal_loss_fixed}
