# 准备数据集
在 config.cfg 配置好“dataset_folder”和“dataset”项后运行 create_train_test_txt.py，这会在代码中设置的 TXT_FOLDER（默认是 ./dataset_txt）下生成四个 txt 文件，分别是“同源/异源”和“训练/测试”。

对数据集的目录结构没有什么要求，create_train_test_txt.py 会递归遍历 dataset_folder/dataset 中的文件夹，找到文件，然后随机匹配同源和异源。

假如已有生成的 txt 文件，但数据集的存放路径已经被改变，可以修改 config.cfg 的对应配置后运行 convert_txt.py 进行修改。*提醒：只是数据集的存放路径被改变这种情况下**不应该**删掉 txt 文件重新生成，而是应该使用 convert_txt.py 修改，不然新生成的同源/异源 txt 文件与原来的并不匹配，影响实验可重现性。*

# 训练和测试
训练：运行 train.py。

测试：运行 test.py。

# 论文
[Finger Vein Verification Algorithm Based on Fully Convolutional Neural Network and Conditional Random Field](https://ieeexplore.ieee.org/document/9052656)
