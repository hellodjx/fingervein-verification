import random
import os
import cv2
import configparser
import numpy as np
from keras.utils import np_utils
from sklearn.model_selection import train_test_split


TXT_FOLDER = "./dataset_txt"
ORIGINAL_TRAIN_DIR = "./data"

# TODO 该函数仅用于临时实验，考虑移除或完善这个函数
def get_original(x):
    if not isinstance(x, str):
        l = []
        for i in x:
            split_path_list = i.split("/")[-3:]
            split_path = "/".join(split_path_list)
            original_path = os.path.join(ORIGINAL_TRAIN_DIR, split_path)
            temp = list(os.path.splitext(original_path))
            temp[1] = ".bmp"
            num = str(int(temp[0].split("_")[-1].split(".")[0]) + 1)
            num = num.zfill(2)

            split_path_list = temp[0].split("/")
            split_path_list[-1] = num
            temp[0] = "/".join(split_path_list)

            original_path = "".join(temp)          
            l.append(original_path)
        return l
    else:
        split_path_list = x.split("/")[-3:]
        split_path = "/".join(split_path_list)
        original_path = os.path.join(ORIGINAL_TRAIN_DIR, split_path)
        return original_path


def get_txt_file_path(file_name):
    config = configparser.ConfigParser()
    config.read("./config.cfg")
    dataset_name = config["SETTINGS"]['dataset']

    return os.path.join(TXT_FOLDER, dataset_name + "_" + file_name)


def read_txt(fname):
    with open(fname, 'r') as f:
        samples = f.readlines()
        samples = [item.rstrip('\n').split(';')
                   for item in samples]
        return samples


def load_tra_val_path(data_input_type):
    neg_samples = read_txt(get_txt_file_path('train_negtive_pairs.txt'))
    pos_samples = read_txt(get_txt_file_path('train_positive_pairs.txt'))
    images = []
    labels = []

    print(len(neg_samples), len(pos_samples))

    if data_input_type == "general":
        for index, value in enumerate([neg_samples, pos_samples]):
            for item in value:
                images.append(item)
                labels.append(index)
    else:
        for index, value in enumerate([neg_samples, pos_samples]):
            for item in value:
                images.append((item, get_original(item)))
                labels.append(index)

    state = np.random.get_state()
    np.random.shuffle(images)
    np.random.set_state(state)
    np.random.shuffle(labels)

    X_train, X_valid, y_train, y_valid = train_test_split(
        images, labels, test_size=0.3, random_state=random.randint(0, 100))

    return X_train, X_valid, y_train, y_valid


def get_im_cv2(paths, img_rows, img_cols, color_type=1, normalize=True):
    '''
    参数：
        paths：要读取的图片路径列表
        img_rows:图片行
        img_cols:图片列
        color_type:图片颜色通道
    返回: 
        imgs: 图片数组
    '''
    left_imgs = []
    right_imgs = []
    for path in paths:
        if color_type == 1:
            left_img = cv2.imread(path[0], 0)
            right_img = cv2.imread(path[1], 0)
        elif color_type == 3:
            left_img = cv2.imread(path[0])
            right_img = cv2.imread(path[1])

        left_resized = cv2.resize(left_img, (img_cols, img_rows))
        right_resized = cv2.resize(right_img, (img_cols, img_rows))

        if normalize:
            left_resized = left_resized.astype('float32')
            right_resized = right_resized.astype('float32')

            left_resized /= 255
            right_resized /= 255

        left_imgs.append(left_resized)
        right_imgs.append(right_resized)

    left_imgs = np.array(left_imgs).reshape(
        len(paths), img_rows, img_cols, color_type)
    right_imgs = np.array(right_imgs).reshape(
        len(paths), img_rows, img_cols, color_type)

    return left_imgs, right_imgs


def get_train_batch(X_train, y_train, batch_size, img_w, img_h, color_type, is_argumentation, data_input_type):
    '''
    参数：
        X_train：所有图片路径列表
        y_train: 所有图片对应的标签列表
        batch_size:批次
        img_w:图片宽
        img_h:图片高
        color_type:图片类型
        is_argumentation:是否需要数据增强（未实现）
    返回: 
        一个generator，x: 获取的批次图片 y: 获取的图片对应的标签
    '''
    while 1:
        if data_input_type == "general":
            for i in range(0, len(X_train), batch_size):
                x_left, x_right = get_im_cv2(
                    X_train[i:i+batch_size], img_w, img_h, color_type)
                y = y_train[i:i+batch_size]
                y = np.array(y)
                y = np_utils.to_categorical(y, 2)
                if is_argumentation:
                    pass

                yield [x_left, x_right], y
        elif data_input_type == "four_image":
            for i in range(0, len(X_train), batch_size):
                non_original_train = list(map(lambda x:x[0], X_train[i:i+batch_size]))
                original_train = list(map(lambda x:x[1], X_train[i:i+batch_size]))
                non_original_left, non_original_right = get_im_cv2(
                    non_original_train, img_w, img_h, color_type
                )
                original_left, original_right = get_im_cv2(
                    original_train, img_w, img_h, color_type
                )
                y = y_train[i:i+batch_size]
                y = np.array(y)
                y = np_utils.to_categorical(y, 2)
                if is_argumentation:
                    pass

                yield [non_original_left, non_original_right, original_left, original_right], y
